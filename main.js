console.log('Loading libraries');
var express = require('express');

console.log('Loading config.json');
var config = require('./config.json');

console.log('Creating express app');
var app = express();
app.use('/', express.static(config.paths.base));
app.use('/middler.js', express.static(config.paths.middler));
app.use('/redirect.html', express.static(config.paths.redirect));

console.log('Starting server');
app.listen(config.port);
console.log('Listening on ' + config.port);
