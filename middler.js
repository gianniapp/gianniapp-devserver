version.middler = '1.0.0';
middlerName = 'gianniapp-devserver';
opensource['gianniapp-devserver'] = opensourcelicenses.mit('Marco Benzoni', 2018);

var middler = {
  features: {
    externalDarkControl: false,
    packageManagement: false,
    syncRedirect: true
  },
  getFeatureInfo(name) {
    if (name == 'externalDarkControl') {
      return true; // sample, turn middler.features.externalDarkControl to true to use this
    } else if (name == 'syncRedirect') {
      return window.location.origin + '/redirect.html';
    }
  },
  getLang: function() {
    if (window.location.hash == "#" || window.location.hash == "") {
      var goToLang = knownLangs[0];
      if (typeof navigator.language != 'undefined') {
        var desiredLang = navigator.language.split('-')[0].toLowerCase();
        if (knownLangs.indexOf(desiredLang) > -1) {
          goToLang = desiredLang;
        }
      }
      middlerInternal.changingHashes = true;
      window.location.hash = '#' + goToLang;
      return goToLang;
    } else {
      return window.location.hash.substr(1);
    }
  },
  setLang: function(lang) {
    middlerInternal.changingHashes = true;
    window.location.hash = '#' + lang;
  },
  dialog: function(title, text, button, callback) {
    middler.dialogMultiple(title, text, [button], function(_i) {
      callback();
    })
  },
  dialogMultiple: function(title, text, btns, callback) {
    $('#middler-dialogBtn-title').html(title);
    $('#middler-dialogBtn-text').html(text.replace(/\n/g, '<br>'));
    $('#middler-dialogBtn-btns').html('');
    var clickGen = function(i) {
      return function() {
        $('#middler-dialogBtn')[0].close();
        callback(i);
      }
    }
    for (var i in btns) {
      var button = document.createElement('button');
      $(button).html(btns[i]);
      $(button).on('click', clickGen(i));
      $('#middler-dialogBtn-btns').append(button);
    }
    $('#middler-dialogBtn')[0].showModal();
  },
  dialogText: function(title, text, callback) {
    $('#middler-dialogText-title').html(title);
    $('#middler-dialogText-text').html(text.replace(/\n/g, '<br>'));
    $('#middler-dialogText-input').val('');
    middlerInternal.storedTextCallback = callback;
    $('#middler-dialogText')[0].showModal();
  },
  url: function(url) {
    window.open(url);
  },
  quit: function() {
    window.close();
    $('#content').hide();
    $('#topBar').slideUp();
    middler.dialog(_('middlerWeb.quit.title'), _('middlerWeb.quit.text'), _('middlerWeb.quit.restart'), function() {
      document.body.innerHTML = '';
      loader(true);
      setTimeout(function() {
        location.reload()
      }, 100);
    });
  },
  data: {
    get: function(key) {
      return window.localStorage.getItem(key)
    },
    set: function(key, value) {
      return window.localStorage.setItem(key, value)
    },
    delete: function(key) {
      if (key === null) {
        return window.localStorage.clear();
      } else {
        return window.localStorage.removeItem(key)
      }
    }
  }
}

var middlerInternal = {
  changingHashes: false,
  storedTextCallback: function(x) {}
}

window.addEventListener('full-load', function() {
  $(window).on('hashchange', function() {
    if (middlerInternal.changingHashes) {
      middlerInternal.changingHashes = false;
    } else {
      window.dispatchEvent(new Event('lang-change'));
    }
  })
});

window.addEventListener('message', function(event) {
  if (event.origin != window.location.origin) return;
  console.log(event.data);
  if (event.data.gianniSyncSetupCode) {
    var dataStr = atob(event.data.gianniSyncSetupCode);
    var data = JSON.parse(dataStr);
    setupSync(data);
  }
})

var dialog = document.createElement('dialog');
dialog.setAttribute('id', 'middler-dialogBtn');
var title = document.createElement('h3');
title.setAttribute('id', 'middler-dialogBtn-title');
dialog.appendChild(title);
var text = document.createElement('p');
text.setAttribute('id', 'middler-dialogBtn-text');
dialog.appendChild(text);
var buttons = document.createElement('div');
buttons.setAttribute('id', 'middler-dialogBtn-btns');
dialog.appendChild(buttons);
document.body.appendChild(dialog);

var dialogText = document.createElement('dialog');
dialogText.setAttribute('id', 'middler-dialogText');
var titleText = document.createElement('h3');
titleText.setAttribute('id', 'middler-dialogText-title');
dialogText.appendChild(titleText);
var textText = document.createElement('p');
textText.setAttribute('id', 'middler-dialogText-text');
dialogText.appendChild(textText);
var inputText = document.createElement('input');
inputText.type = 'text';
inputText.setAttribute('id', 'middler-dialogText-input');
inputText.addEventListener('keypress', function(e) {
  if(e.keyCode == 13) {
    var val = $('#middler-dialogText-input').val();
    if (val == "") {
      middlerInternal.storedTextCallback(false);
    } else {
      middlerInternal.storedTextCallback(val);
    }
    $('#middler-dialogText')[0].close();
  }
});
dialogText.appendChild(inputText);
dialogText.appendChild(document.createElement('br'));
var buttonOk = document.createElement('button');
buttonOk.setAttribute('id', 'middler-dialogText-ok');
buttonOk.setAttribute('data-i18n', 'ok');
buttonOk.addEventListener('click', function() {
  var val = $('#middler-dialogText-input').val();
  if (val == "") {
    middlerInternal.storedTextCallback(false);
  } else {
    middlerInternal.storedTextCallback(val);
  }
  $('#middler-dialogText')[0].close();
});
dialogText.appendChild(buttonOk);
var buttonCancel = document.createElement('button');
buttonCancel.setAttribute('id', 'middler-dialogText-cancel');
buttonCancel.setAttribute('data-i18n', 'cancel');
buttonCancel.addEventListener('click', function() {
  $('#middler-dialogText')[0].close();
  middlerInternal.storedTextCallback(false);
});
dialogText.appendChild(buttonCancel);
document.body.appendChild(dialogText);

window.dispatchEvent(new Event('framework-ready'));
